const clone = require('clone')
const config = require('./config')

const db = {}

const defaultData = {
  contacts: [
    {
      email: 'irtizamahmud79@gmail.com',
      password: '123456'
    },
    {
      email: 'irtizamahmud79@outlook.com',
      password: '123abc'
    },
    {
      email: 'irtizamahmud@gmail.com',
      password: 'qwert12'
    }
  ]
}

const get = (token) => {
  let data = db[token]

  if (data == null) {
    data = db[token] = clone(defaultData)
  }

  return data
}

const add = (token, contact) => {
  if (!contact.id) {
    contact.id = Math.random().toString(36).substr(-8)
  }

  get(token).contacts.push(contact)

  return contact
}

const remove = (token, id) => {
  const data = get(token)
  const contact = data.contacts.find(c => c.id === id)

  if (contact) {
    data.contacts = data.contacts.filter(c => c !== contact)
  }

  return { contact }
}

const emailFind = (token, contact) => {
  const data = get(token)
  const response = data.contacts.find(c => c.email === contact.email && c.password == contact.password)
  return { response }
}

module.exports = {
  get,
  add,
  remove,
  emailFind
}
