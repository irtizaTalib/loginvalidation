import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';

class ListContacts extends Component {
  static propTypes = {
    contacts: PropTypes.array.isRequired,
    onDeleteContact: PropTypes.func.isRequired
  };

  state = {
    query: ""
  };

  updateQuery = (query) => {
    this.setState(()=> ({
      query : query.trim()
    }))
  }

  clearQuery = () =>  {
    this.updateQuery('')
  }


  render() {
    const { query } = this.state
    const { contacts , onDeleteContact } = this.props

    const showingContacts = query === '' ? contacts : contacts.filter((c) => (c.name.toLowerCase().includes(query.toLocaleLowerCase()) )) 

    return (
      <div className="list-contacts">
        <div className="list-contacts-top">
          <input 
            className="search-contacts" 
            type = "text"
            placeholder = "Search-contacts"
            value = {query} 
            onChange = {(event) => this.updateQuery(event.target.value)}
            />
            <Link 
              to = './create'
              className= 'add-contact'
              >
                Add Contact
            </Link>
        </div>
    
        {showingContacts.length !== contacts.length && 
        (
          <div className="showing-contacts">
            <span> Now Showing {showingContacts.length}  of {contacts.length} </span>
            <button onClick = {this.clearQuery}> Show all </button>  
          </div>
        )}      

        <ul className="contact-list">
          {showingContacts.map(person => (
            <li key={person.id} className="contact-list-item">
              <div className="contact-avater">
                <img
                  src={`${person.avatarURL}`}
                  width="80px"
                  style={{ borderRadius: "50%" }}
                  alt="person"
                ></img>
              </div>
              <div className="contact-details">
                <p>{person.id}</p>
                <p>{person.password}</p>
              </div>
              <button
                onClick={() => onDeleteContact(person)}
                className="contact-remove"
              >
                Remove
              </button>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default ListContacts;
