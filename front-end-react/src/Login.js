import React, { Component } from "react";
import { Link } from "react-router-dom";
import ImageInput from "./ImageInput";
import serializeForm from 'form-serialize';

class Login extends Component {
    handleSubmit = (e) => {
        e.preventDefault()
        const values = serializeForm (e.target , {hash: true})

        if(this.props.onLogin) {
            this.props.onLogin(values)
        }
    }

  render() {
    return (
      <div className="form-container" >
        {/* <Link className="close-create-contact" to="/">
          Close
        </Link> */}
        <form onSubmit={this.handleSubmit} className="create-contact-form">
          <ImageInput
            className="create-contact-avatar-input"
            name="avatarURL"
            maxHeight={64}
          />
          <div className='create-contact-details'>
              <input type='email' name='name' placeholder='email' />
              <input type='password' name='handle' placeholder='password' />
              <button>Log in</button>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;
