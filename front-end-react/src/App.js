import React, { Component } from "react";
import ListContacts from "./ListContacts";
import * as ContactsAPI from "./utils/ContactsAPI";
import Login from "./Login";
import { Route } from "react-router-dom";

class App extends Component {
  state = {
    contacts: [],
    response: false,
  };
  componentDidMount() {
    ContactsAPI.getAll().then((contacts) => {
      this.setState(() => ({
        contacts,
      }));
    });
  }

  removeContact = (contact) => {
    this.setState((currentState) => ({
      contacts: currentState.contacts.filter((c) => {
        return c.id !== contact.id;
      }),
    }));
    ContactsAPI.remove(contact);
  };
  validateLogIn = (contact) => {
    ContactsAPI.getAll(contact)
    .then((response) => {
      console.log("response" + response)
    })
  };

  createContact = (contact) => {
    ContactsAPI.create(contact).then((contact) => {
      this.setState((currentState) => ({
        contacts: currentState.contacts.concat([contact]),
      }));
    });
  };

  render() {
    return (
      <div className="App">
        <Route
          exact
          path="/"
          render={() => (
            <Login
              // onLogin={(contact) => {
              //   this.removeContact;
              // }}
              onLogin={this.validateLogIn}
            />
          )}
        />

        <Route
          path="/landing"
          render={({ history }) => (
            <ListContacts
              contacts={this.state.contacts}
              onDeleteContact={this.removeContact}
            />
          )}
        />
      </div>
    );
  }
}

export default App;
